
import './App.css';
import HeroSection from './components/Hero/HeroSection'
import Whatdo from './components/Whatdo/Whatdo'
import FardadBanner from './components/FardadBanner/FardadBanner'
import WhatDone from './components/WhatDone/WhatDone'
import WhyUs from './components/WhyUs/WhyUs'
import WhoUs from './components/WhoUs/WhoUs'
import WhoKnow from './components/WhoKnow/WhoKnow'
import Contact from './components/Contact/Contact'
import Footer from './components/Footer/Footer'
function App() {
  return (
    <div className="overflow-x-hidden">
      <HeroSection />
      <Whatdo />
      <FardadBanner />
      <WhatDone />
      <WhyUs />
      <WhoUs />
      <WhoKnow />
      <Contact />
      <Footer />
    </div>
  );
}

export default App;
