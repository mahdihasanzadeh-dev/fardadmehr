import React from 'react'
import featureImage from '../../asstes/img/features-img-1.png';
import '../../App.css' ;
import './FardadBanner.css'
function FardadBanner() {
    return (
        <div className="banner-containrt">
            <div className="row">
                <div className="grid grid-cols-2 gap-2">
                    <div className="flex items-center justify-center">
                        <img src={featureImage} alt="banner" />
                    </div>
                    <div className="flex flex-col items-start justify-center">                        
                        <h1 className="text-brand text-2xl  font-persian sm:text-5xl font-black mb-8">ظاهری زیبا</h1>
                        <h2 className="text-black text-2xl  font-persian sm:text-6xl font-black">
                          با موتوری قدرتمند                        
                        </h2>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default FardadBanner
