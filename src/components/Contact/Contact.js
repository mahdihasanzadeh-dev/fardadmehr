import React from 'react'
import addressIcon from '../../asstes/img/address.png';
import phoneIcon from '../../asstes/img/phone.png';
import emailIcon from '../../asstes/img/email.png';
import '../../App.css'
import '../WhatDone/WhatDone.css'
function Contact() {
    return (
        <div className="row">
              <div className="what-done-wrapper">
                  <div className="grid grid-cols-1 sm:grid-cols-3 gap-2">
                      <div className="contact-item">
                          <span className="icon gray">
                              <img src={addressIcon} alt="addressIcon" />
                          </span>
                          <div className="text">
                         خراسان جنوبی - بیرجند - خیابان غفاری - ساختمان شماره 2 پارک علم وفناوری
                          </div>
                      </div>
                      <div className="contact-item">
                          <span className="icon gray">
                              <img src={phoneIcon} alt="phoneIcon" />
                          </span>
                          <div className="text">
                          ۲۲۵۰۵۶۶۱ - ۲۲۳۲۴۴۷۲
                          </div>
                      </div>
                      <div className="contact-item">
                          <span className="icon gray">
                              <img src={emailIcon} alt="emailIcon" />
                          </span>
                          <div className="text text-blue-500">
                            hello@fardadmehr.com
                          </div>
                      </div>
                  </div>
              </div>
        </div>
    )
}

export default Contact
