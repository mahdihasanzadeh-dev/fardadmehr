import React from 'react'

function Divider() {
    return (
        <div className=" absolute  z-10 bottom-0  overflow-x-hidden w-full flex items-center justify-center">
            <img src="https://yasna.team/modules/yasnateam/images/divider-white.svg" alt="divider" />
        </div>
    )
}

export default Divider
