import React from 'react'
import  '../../App.css'
import './Slagon.css';
import homeBanner from '../../asstes/img/home-banner.png';
function Slogan() {
    return (
        <div className="row">
            <div className="slogon-wrapper">
                <div className=" flex  items-center justify-center flex-col mt-18 sm:mt-0"> 
                    <h1 className="text-brand font-persian text-3xl sm:text-3xl md:text-4xl font-black mb-8">فرداد مهر</h1>
                    <h2 className="text-white text-center font-persian text-3xl sm:text-4xl md:text-5xl font-black">
                        موتوری پر قدرت 
                        با ظاهری زیبا
                    </h2>
                </div>
                <div className="slogon-items justify-end">
                    <img src={homeBanner} alt="asd" className="h-96" />
                </div>
            </div>
        </div>
       
    )
}

export default Slogan
