import React from 'react'
import securityImage from '../../asstes/img/security.png';
import autoUpdate from '../../asstes/img/auto-update.png';
import zeroDefect from '../../asstes/img/zero-defects.png';
import exclusiveSolution from '../../asstes/img/solution.png';
import zeroToHero from '../../asstes/img/zero-to-hert.png';
import document from '../../asstes/img/document.png';
import '../../App.css'
import '../WhatDone/WhatDone.css'
function WhyUs() {
    return (
        <div className="row" id="whyus">
            <div className="what-done-wrapper">
                <div className="flex justify-center items-center w-full">
                    <h1 className="font-persian text-2xl sm:text-4xl text-center font-black text-blue-800 mb-4 ">چرا ما؟</h1>
                </div>
                <div className="grid grid-cols-1 sm:grid-cols-3 gap-4">
                    <div className="features">
                        <div className="icon blue">
                            <img src={autoUpdate} alt="asd"  />
                        </div>
                        <h3>بروزرسانی خودکار</h3>
                        <p>
                        توسعه‌پذیر بودن محصولات فرداد مهر تضمین می‌کند که با اتصال دائم به ابر فرداد مهر، از قابلیت دریافت به‌روزرسانی‌های امنیتی و بهره‌گیری از قابلیت‌های جدید برخوردار باشند و در مدت پشتیبانی، همیشه به آخرین نسخه از تکنولوژی‌های استفاده‌شده مجهز شوند.
                        </p>
                    </div>
                    <div className="features">
                        <div className="icon red">
                            <img src={securityImage} alt="asd"  />
                        </div>
                        <h3>امنیت بالا</h3>
                        <p>
                        محصولات فرداد مهر چه از هستهٔ قدرتمند فردادوب استفاده کنند و چه نکنند، بر پایهٔ فن‌آوری‌هایی نوشته می‌شوند که از امنیت بالایی برخوردارند. ما هم مثل شما، از سایت‌سازهای پیش‌ساخته که هزار کار می‌کنند و هیچ کاری را درست انجام نمی‌دهند بیزاریم.
                        </p>
                    </div>
                    <div className="features">
                        <div className="icon green">
                            <img src={zeroDefect} alt="asd"  />
                        </div>
                        <h3>بدون عیب و نقص </h3>
                        <p>
                        آنچه از فرداد مهر تحویل می‌گیرید، همان است که در مستندات وصف شده. پس از پایان زمان تست و تحویل محصول نهایی، عیب و ایرادی در آن نخواهید یافت. یک دستور، یک اجرا.
                        </p>
                    </div>
                    <div className="features">
                        <div className="icon purple">
                                <img src={exclusiveSolution} alt="asd"  />
                        </div>
                        <h3>راهکار اختصاصی</h3>
                        <p>
                        مشاوران فرداد مهر نیاز شما را می‌شنوند و ارزیابی می‌کنند و راهکاری مخصوص خود شما ارائه می‌دهند که با اهداف و فرصت‌ها و موانع پیش روی شما سازگاری داشته باش
                        </p>
                    </div>
                    <div className="features">
                        <div className="icon teal">
                                <img src={zeroToHero} alt="asd"  />
                        </div>
                        <h3>صفر تا صد</h3>
                        <p>
                        ساخت سایت خوب، تازه اول راه است. اجارهٔ نشانی اینترنتی، تأمین سرور میزبان، تولید محتوا، مدیریت شبکه‌های اجتماعی، پشتیبانی خدمات، و هزار مانع دیگر در مسیر است. فرداد مهر تنهایتان نمی‌گذارد
                        </p>
                    </div>
                    <div className="features">
                        <div className="icon orange">
                                <img src={document} alt="asd"  />
                        </div>
                        <h3>مستندات حرفه‌ای</h3>
                        <p>
                        همه چیز را مستند می‌کنیم تا نقطهٔ تاریکی در پروژه باقی نماند. ما نسبت به اجرای تعهدات و رعایت محدودیت‌های زمانی ذکرشده در قرارداد وفاداریم و وفادار خواهیم ماند.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default WhyUs
