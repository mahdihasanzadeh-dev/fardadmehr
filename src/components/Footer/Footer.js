import React from 'react'
import '../../App.css'
import '../WhatDone/WhatDone.css'
import logo from '../../asstes/img/fardad.jpg'
function Footer() {
    return (
        <div className="w-screen bg-blue-500">
            <div className="row">
                <div className="what-done-wrapper footer-wrapper">
                    <img src={logo} alt="Logo" className=" w-32 h-32" />
                    <div className="social">
                        <span><a href="https://www.instagram.com/reactapp_ir/">اینستاگرام</a></span>
                        <span><a href="https://t.me/reactapp_ir">تلگرام</a></span>
                        <span><a href="https://twitter.com/Reactappiran">توییتر</a></span>
                    </div>
                </div>
            </div>
            <div className="w-screen bg-blue-700 py-6 flex items-center justify-center">
                <span className="text-center text-white font-persian">
                     حق و حقوق مادی و معنوی برای فرداد مهر تیم محفوظ است. 1400
                </span>
            </div>
        </div>
    )
}

export default Footer
