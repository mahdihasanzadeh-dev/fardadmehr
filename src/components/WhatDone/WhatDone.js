import React from 'react'
import portfolioImage1 from '../../asstes/img/portfolio-modal-1.png';
import portfolioImage2 from '../../asstes/img/portfolio-image-2.png';
import portfolioImage3 from '../../asstes/img/portfolio-image-3.png';
import portfolioImage4 from '../../asstes/img/portfolio-image-4.png';
import '../../App.css'
import './WhatDone.css'
function WhatDone() {
    return (
        <div className="row" id="whatdone">
            <div className="what-done-wrapper">
                <div className="flex justify-center items-center w-full">
                    <h1 className="font-persian text-2xl sm:text-4xl text-center font-black text-blue-800 ">چه کرده ایم؟</h1>
                </div>
                <div className=" grid grid-cols-1 sm:grid-cols-4 gap-4 mt-4">
                    <div className="project-item">
                        <img src={portfolioImage1} alt="sa" />
                    </div>
                    <div className="project-item">
                        <img src={portfolioImage2} alt="sa" />
                    </div>
                    <div className="project-item">
                        <img src={portfolioImage3} alt="asd" />
                    </div>
                    
                    <div className="project-item">
                        <img src={portfolioImage4} alt="asfas" />
                    </div>
                </div>
            </div>
        </div>
    )
}

export default WhatDone
