import React from 'react'
import marktingImage from '../../asstes/img/markting.png';
import mobileImage from '../../asstes/img/mobile.png';
import brandingImage from '../../asstes/img/branding.png';
import websiteImage from '../../asstes/img/website.png';
import peyPerClick from '../../asstes/img/pay-per-click.png';
import seoImage from '../../asstes/img/seo.png';
import '../../App.css'
import './Whatdo.css'

function Whatdo() {
    return (
        <div className="row" id="whatdo">
            <div className="what-wrapper relative ">
                <div className="flex justify-center items-center w-full">
                    <h1 className="font-persian text-2xl sm:text-4xl text-center font-black text-blue-800">چه می کنیم؟</h1>
                </div>
                <div className=" grid grid-cols-1 gap-1 w-full relative sm:grid-cols-3 sm:gap-4">
                    <div className="what-items">
                        <img src={marktingImage} alt="فروشگاه اینترنتی" className="img-do mb-7" />
                        <h3>  فروشگاه اینترنتی</h3>
                        <a href="#" className="btn-more">بیشتر بدانید</a>
                    </div>
                    <div className="what-items"> 
                        <img src={mobileImage} alt="اپلیکیشن موبایل" className="img-do mb-7" />
                        <h3>اپلیکیشن موبایل</h3>
                        <a href="#" className="btn-more">بیشتر بدانید</a>
                    </div>
                    <div className="what-items">
                        <img src={brandingImage} alt="وب سایت و نرم افزار سازمانی" className="img-do mb-7" />
                        <h3>وب سایت و نرم افزار سازمانی</h3>
                        <a href="#" className="btn-more">بیشتر بدانید</a>
                    </div>
                    <div className="what-items">
                         <img src={websiteImage} alt="دیجیتال مارکتینگ" className="img-do mb-7" />
                         <h3>دیجیتال مارکتینگ</h3>
                         <a href="#" className="btn-more">بیشتر بدانید</a>
                    </div>
                    <div className="what-items">
                       <img src={peyPerClick} alt="طراحی UI و توسعه UX" className="img-do mb-7" />
                       <h3>طراحی UI و توسعه UX</h3> 
                        <a href="#" className="btn-more">بیشتر بدانید</a>
                    </div>
                    <div className="what-items">
                    <img src={seoImage} alt="بهینه سازی جستجو (SEO)" className="img-do mb-7" />
                        <h3>بهینه سازی جستجو (SEO)</h3>
                        <a href="#" className="btn-more">بیشتر بدانید</a>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Whatdo
