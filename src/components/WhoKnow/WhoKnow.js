import React from 'react'
import reactIcon from '../../asstes/img/react-icon.png';
import nodeJsLogo from '../../asstes/img/nodejs-logo.png';
import graphQlLogo from '../../asstes/img/graphql.png';
import laravelIcon from '../../asstes/img/laravel.svg'; 
import mongodbIcon from '../../asstes/img/mongodb-icon.png'; 
import typescriptIcon from '../../asstes/img/typescript-icon.svg'; 
import '../../App.css'
import '../WhatDone/WhatDone.css'
function WhoKnow() {
    return (
        <div className="row">
            <div className="what-done-wrapper">
                <div className="flex justify-start items-center w-full">
                    <h1 className="font-persian text-2xl sm:text-4xl text-center font-black text-blue-800 mb-4 ">  چه می دانیم؟</h1>
                </div>
                <div className="grid grid-cols-1 sm:grid-cols-6 gap-6">
                    <div className="tools">
                        <img src={reactIcon} alt="asd" />
                    </div>
                    <div className="tools">
                        <img src={nodeJsLogo} alt="asasf" />
                    </div>
                    <div className="tools">
                        <img src={graphQlLogo} alt="asd" />
                    </div>
                    <div className="tools">
                        <img src={laravelIcon} alt="react" />
                    </div>
                    <div className="tools">
                        <img src={mongodbIcon} alt="sdf" />
                    </div>
                    <div className="tools">
                        <img src={typescriptIcon} alt="casfa" />
                    </div>
                </div>
            </div>
        </div>
    )
}

export default WhoKnow
