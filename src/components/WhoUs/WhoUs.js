import React from 'react'
import team from '../../asstes/img/team.jpg'
import '../../App.css'
import '../WhatDone/WhatDone.css'
function WhoUs() {
    return (
        <div className="row" id="whous">
            <div className="what-done-wrapper">
                <div className="flex justify-center items-center w-full">
                    <h1 className="font-persian text-2xl sm:text-4xl text-center font-black text-blue-800 mb-4 "> که هستیم ؟</h1>
                </div>
                <div className="grid grid-cols-1 sm:grid-cols-2 gap-6">
                    <div>
                        <img 
                            src={team}
                            alt="FardaMeh team"
                            className="w-full object-cover object-center h-96 rounded-sm shadow-lg"
                        />
                    </div>
                    <div className="about-us">
                        <h2>فرداد مهر تیم</h2>
                        <div className="content">
                            <p>
                            فرداد مهر سازمانی چابک، اما کوچک است با اندیشه‌ها و ارزش‌هایی بزرگ، که با نام رسمی شرکت توسعه فن‌آوری اطلاعات سورنا (سهامی خاص)، از سال ۱۳۸۴ مشغول به کار است.
                            <br />

کار اصلی ما در فرداد مهر، ساخت نرم‌افزار است و در این راه تلاش می‌کنیم از جدیدترین فن‌آوری‌های مقرون‌به‌صرفهٔ دنیا استفاده کنیم، اما از آن مهم‌تر: ما راهکارهای لازم برای ورود شما به دنیای پیچیدهٔ فن‌آوری اطلاعات را پیدا می‌کنیم؛ راهکارهایی که مخصوص خود شما طراحی شده‌اند.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default WhoUs
