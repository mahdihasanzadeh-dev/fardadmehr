import React from 'react'
import Header from '../Header/Header'
import Slogan from '../Slogan/Slogan';
import Divider from '../Divider/Divider';
import './Hero.css'
function HeroSection() {
    return (
        <div className="w-screen flex-col flex relative  mainHeroSection ">
            <Header />
            <Slogan />
            <Divider />
        </div>
    )
}

export default HeroSection
