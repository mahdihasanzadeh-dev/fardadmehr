import React from 'react'
import  '../../App.css'
import './Header.css'

function Header() {
    return (
        <div className="row">
           <div className="header-row">
               <div className="global-logo ">
                   
               </div>
               <div className="nav-menu">
                    <ul className="nav-menu-wrapper">
                        <li><a href="#whatdo">چه می کنیم</a> </li>
                        <li><a href="#whatdone">چه کرده ایم</a></li>
                        <li><a href="#whyus">چرا ما</a></li>
                        <li><a href="#whous">که هستیم</a></li>
                        
                    </ul>
               </div>
           </div>
        </div>
    )
}

export default Header
