module.exports = {
  purge: ['./src/**/*.{js,jsx,ts,tsx}', './public/index.html'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors:{
        brand:{
          DEFAULT:'#ffe657',
          light :"#3fbaeb",
          dark:'#0c87b8'
        },
      },
      fontFamily:{
        persian:'IRANSans'
      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
